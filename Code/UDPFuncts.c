//
// Created by Andrew on 9/2/2019.
//

#include "UDPFuncts.h"
#include <stdio.h>
#include <string.h>

// prints the menu for the client
// when copying from the program's output, clion automatically added the correct newlines and excape sequences!!!
void printClientMenu() {
    printf("\nEnter one of the following commands:\n"
           "\"1\" = List Songs\n"
           "\"2\" = Stream a Song\n"
           "\"3\" = exit\n");
}

// takes in the user input and modified the string pointed to by the second argument to be the correct reply
int parseUserInput(char* inputStr, char* output, char* song){
    int inputNum = -1;
    if (strcmp(inputStr, "1") == 0) {
        inputNum = 1;
    }
    else if (strcmp(inputStr, "2") == 0) {
        inputNum = 2;
    }
    else if (strcmp(inputStr, "3") == 0) {
        inputNum = 3;
    }
    // puts a null as the first character of song just in case its used later and not set, which it shouldn't
    //strcpy(song, "\0");

    // switch case to set output correctly
    // https://www.geeksforgeeks.org/switch-statement-cc/
    switch(inputNum){
        case 1:
            strcpy(output, "LIST_REQUEST");
            break;

        case 2:
            printf("Please enter a song name: ");
            // scanf doesn't work with spaces, and a song file name may have a space, so I followed https://stackoverflow.com/questions/1247989/how-do-you-allow-spaces-to-be-entered-using-scanf
            fflush(stdin);
            getchar();
            fgets(song, 101, stdin);
            if ((strlen(song) > 0) && (song[strlen (song) - 1] == '\n'))
                song[strlen (song) - 1] = '\0';
            //scanf("%101s", song);
            //http://joequery.me/code/snprintf-c/
            snprintf(output, 101, "START_STREAM\n%s", song);
            break;

        case 3:
            strcpy(output, "\0"); //nothing for this case
            break;

        default:
            inputNum = -1;
            break;
    }

    return inputNum;
}
