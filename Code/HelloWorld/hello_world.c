//
// Created by Andrew on 8/29/2019.
//

// Ripped from the example code
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>


int main() {
    printf("Hello World!\n");
    return 0;
}