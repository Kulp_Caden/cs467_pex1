//
// Created by Andrew on 9/2/2019.
//

#ifndef UDP_CLIENT_UDPFUNCTS_H
#define UDP_CLIENT_UDPFUNCTS_H

// prints the menu for the client
void printClientMenu();

// takes in the user input and modified the string pointed to by the second argument to be the correct reply, return the mode the user chose
//Returns int 1 for string input 1, 2 for 2, 3 for 3 and -1 or error
// if 2 is inputted, the function prompts for the song name and writes is to the song
int parseUserInput(char* inputStr, char* output, char* song);

#endif //UDP_CLIENT_UDPFUNCTS_H
