//
// Created by Andrew on 8/29/2019.
//

// NON-WEBSITE HELP
// ask zach how some of my data could have changed when sending it over the network, he said string operations often
// change the data, so I decided to store the frame recieved as buffer and copy it to data, one to do str operations
// on, like parsing the command, and the other to put in the file

// Ripped from the example code

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include "UDPFuncts.h"


#define PORT     4240
#define MAXLINE 1024
#define MAXREQUEST 102

int main() {

    // Copied from example code, edited
    int sockfd; //Socket descriptor, a file descriptor for the socket (cause everything's a file)
    char buffer[MAXLINE]; //buffer to store message from server, MIGHT NEED TO MAKE THIS BIGGER
    struct sockaddr_in servaddr;  //we don't bind to a socket to send UDP traffic, so we only need to configure server address

    // Creating socket file descriptor
    // socket() returns the fd of the socket, -1 on error
    // SOCKET_DGRAM is the standard for UDP connections as they send datagrams and don't make streams (SOCK_STREAM)
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Filling server information
    servaddr.sin_family = AF_INET; //IPv4
    servaddr.sin_port = htons(
            PORT); // port, converted to network byte order (prevents little/big endian confusion between hosts)
    servaddr.sin_addr.s_addr = INADDR_ANY; //localhost, normally you perform a similar operation to the ports on the line above, transform the address to network byte order from just a string of the address with a similar function


    int n, len = sizeof(servaddr);
    int len2 = len;


    // make a timeout for recvfrom
    struct timeval timeout = {2, 0}; //set timeout for 2 seconds
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));


    char exitChar = 'w';
    int errorCount = 0; //exit loop if too many errors
    char clientRequestString[102];
    char userInput[102];
    char song[102];
    char data[MAXLINE];
    int userRequest;
    int shouldNetwork = 1; // 1 for yes to recieve a datagram, 0 for no to recieve
    int frameCount = 1;


    while (exitChar != 113 && errorCount < 2) { //http://www.asciitable.com/ 113 is q

        // get the user choice
        printClientMenu();
        fflush(stdin); // clear stdin buffer
        scanf("%101s", userInput);

        // parse the user's choice and prep the request
        userRequest = parseUserInput(userInput, clientRequestString, song);

        // List the Songs or make the stream request
        if (userRequest == 3) {
            exitChar = 113;\
            shouldNetwork = 0;
        } else if (userRequest == -1) {
            printf("Please enter a valid choice.\n");
            shouldNetwork = 0;
        } else if (userRequest == 1 || userRequest == 2) {
            shouldNetwork = 1;
            sendto(sockfd, (char *) clientRequestString, MAXREQUEST, 0, (struct sockaddr *) &servaddr, len);
        }

        if (shouldNetwork != 0) {
            // receive the song
            if (userRequest == 2) {
                int notDone = 1;
                frameCount = 1;

                // make file for song to be written to
                // file io from https://www.tutorialspoint.com/cprogramming/c_file_io.htm
                FILE *songFile;
                songFile = fopen(song, "wb+");
                if (songFile == NULL) {
                    puts("Error while opening file");
                    notDone = 0;
                    exit(-1);
                }

                while (notDone) {
                    // get the string of the thing being written
                    if ((n = recvfrom(sockfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len2)) < 0) {
                        perror("ERROR");
                        printf("Errno: %d. \n", errno); //error number 11 indicates a timeout
                        if (errno == 11) {
                            //https://stackoverflow.com/questions/13554691/errno-11-resource-temporarily-unavailable?rq=1
                            printf("Network timeout, no packets received, retrying.\n");
                        }
                    } else {

                        memcpy(data, buffer, n); //strncpy edits the data so I hear from Zach, so I backed up the data
                    }

                    // checks to see if there is an error
                    if (strncmp(buffer, "COMMAND_ERROR", 13) == 0) {
                        printf("Error. Song not recognized.\n");
                        notDone = 0;
                    }
                    // if it's a data packet, strip the header then append it to the file
                    else if (strncmp(buffer, "STREAM_DATA", 11) == 0) {
                        // so I'm moving 11 bytes up because of the STREAM_DATA and one byte for the newline
                        fwrite((data + 12 * sizeof(char)), n - 12, 1, songFile); // HOPE THIS POINTER MATH WORKS
                        printf("Frame %d received\t%u Bytes\n", frameCount, n-12);
                        frameCount++;
                    }
                        // if its a terminate packet, set notDone
                    else if (strncmp(buffer, "STREAM_DONE", 11) == 0) {
                        notDone = 0;
                    }
                }

                // close the file
                fclose(songFile);
            } else if (userRequest == 1) {
                // So this hangs if you don't get a message, gonna try to set a timeout based on https://stackoverflow.com/questions/13547721/udp-socket-set-timeout
                if ((n = recvfrom(sockfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &servaddr, &len2)) < 0) {
                    perror("ERROR");
                    printf("Errno: %d. \n", errno); //error number 11 indicates a timeout
                    if (errno == 11) {
                        //https://stackoverflow.com/questions/13554691/errno-11-resource-temporarily-unavailable?rq=1
                        printf("Network timeout, no packets recieved, retrying.\n");
                    }
                    //exit(EXIT_FAILURE);
                } else {
                    buffer[n] = '\0'; //terminate message
                    if (strcmp(buffer, "COMMAND_ERROR") == 0) { // if you have a command error
                        printf("Error\n");
                    }
                    printf("%s\n", (buffer + 11));
                }
            }
        }

    }

    close(sockfd);
    printf("Closed Socket!\n");

    return 0;
}